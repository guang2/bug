# Bug (for Android)

[![](https://jitpack.io/v/org.bitbucket.guang2/bug.svg)](https://jitpack.io/#org.bitbucket.guang2/bug)

> only support Kotlin development

a simple log util

## Usage

### Import from Gradle
Modify your root build.gradle

```groovy
allprojects {
	repositories {
		...
		maven { url 'https://jitpack.io' }
	}
}
```

Then add the the dependency to your app modules

```groovy
dependencies {
    compile 'org.bitbucket.guang2:bug:versionName'
}
```

### Print log

```Kotlin
Bug.d("hello")
```
