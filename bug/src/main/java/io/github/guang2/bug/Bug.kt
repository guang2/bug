package io.github.guang2.bug

import android.util.Log

/**
 * Print Log Util
 * Created by ray on 18-1-20.
 */

object Bug {
    const val ASSERT = 7
    const val ERROR = 6
    const val WARN = 5
    const val INFO = 4
    const val DEBUG = 3
    const val VERBOSE = 2
    const val NONE = 10
    const val ALL = 0

    private var level = ALL

    var TAG = "Bug"
        private set

    fun init(logLevel: Int = ALL, customTag: String = "Bug") {
        TAG = customTag
        level = logLevel
    }

    // @JvmStatic
    fun v(str: String) {
        if (level <= VERBOSE) Log.v(TAG, str)
    }

    fun v(str: String, t: Throwable) {
        if (level <= VERBOSE) Log.v(TAG, str, t)
    }

    fun d(str: String) {
        if (level <= DEBUG) Log.d(TAG, str)
    }

    fun d(str: String, t: Throwable) {
        if (level <= DEBUG) Log.d(TAG, str, t)
    }


    fun i(str: String, t: Throwable) {
        if (level <= INFO) Log.i(TAG, str, t)
    }

    fun i(str: String) {
        if (level <= INFO) Log.i(TAG, str)
    }

    fun w(str: String, t: Throwable) {
        if (level <= WARN) Log.w(TAG, str, t)
    }

    fun w(str: String) {
        if (level <= WARN) Log.w(TAG, str)
    }

    fun e(str: String) {
        if (level <= ERROR) Log.e(TAG, str)
    }

    fun e(str: String, t: Throwable) {
        if (level <= ERROR) Log.e(TAG, str, t)
    }

    fun wtf(str: String) {
        if (level != NONE) Log.wtf(TAG, str)
    }

    fun wtf(str: String, t: Throwable) {
        if (level != NONE) Log.wtf(TAG, str, t)
    }

}