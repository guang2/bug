package io.github.guang2.bug.demo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.github.guang2.bug.Bug

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Bug.init(Bug.NONE, "_demo")
        Bug.i("hello i")
        Bug.d("hello d")
        Bug.e("hello e")
        Bug.w("hello w")
        Bug.v("hello v")
        Bug.wtf("hello wtf")
    }
}
